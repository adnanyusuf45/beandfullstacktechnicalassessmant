package jawabansoalprogramming;

public class Soal_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] angka= new int[6];
		angka[0] = -5;
		angka[1]= 12;
		angka[2] = 11;
		angka[3] = -25;
		angka[4]= 2;
		angka[5] = 12345;
		
		//proses sorting dari kecil ke besar
        int cetak1=0;
        for (int i = 0; i < angka.length; i++) {
            for (int j = i; j < angka.length; j++) {
                if(angka[i]>angka[j]) {
                    cetak1=angka[i];
                    angka[i]=angka[j];
                    angka[j]=cetak1;
                }
            }

        }
        
        System.out.println("Angka Terbesar: "+angka[angka.length-1]);
        System.out.println("Angka Terkecil: "+angka[0]);
	}

}
