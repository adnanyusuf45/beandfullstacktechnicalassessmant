package jawabansoalprogramming;

import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Soal_3 {
	
	 public void countDupChars(String str){
		 
		    //Create a HashMap 
		    Map<Character, Integer> map = new HashMap<Character, Integer>(); 
		 
		    //Convert the String to char array
		    str=str.toUpperCase();
		    char[] chars = str.toCharArray();
		 
		    /* logic: char are inserted as keys and their count
		     * as values. If map contains the char already then
		     * increase the value by 1
		     */
		    
		    boolean checkduplicate=false;
		    for(Character ch:chars){
		      if(map.containsKey(ch)){
		         map.put(ch, map.get(ch)+1);
		         checkduplicate=true;
		      } else {
		         map.put(ch, 1);
		        }
		    }
		 
		    //Obtaining set of keys
		    Set<Character> keys = map.keySet();
		 
		    /* Display count of chars if it is
		     * greater than 1. All duplicate chars would be 
		     * having value greater than 1.
		     */
		    if(checkduplicate==true) {
		    	System.out.println("Duplikat:");
			    for(Character ch:keys){
				      if(map.get(ch) > 1){
				        System.out.println(ch+": "+map.get(ch));
				      }
				}
		    }
		    else {
		    	System.out.println("Duplikat: 0");
		    }

		  }
		 
		  public static void main(String a[]){
			Soal_3 obj = new Soal_3();
			Scanner s=new Scanner(System.in);
			System.out.print("Input: ");
	        String string=s.next();
	        System.out.println("Kata: "+string);
	        
		    obj.countDupChars(string);
		  }
}
